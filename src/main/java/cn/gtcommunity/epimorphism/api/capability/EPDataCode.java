package cn.gtcommunity.epimorphism.api.capability;

public class EPDataCode {

    //  Custom DataCode
    //TODO 替换可读性更高的命名
    public static final int EP_CHANNEL_1 = 9984;
    public static final int EP_CHANNEL_2 = 9985;
    public static final int EP_CHANNEL_3 = 9986;
    public static final int EP_CHANNEL_4 = 9987;
    public static final int EP_CHANNEL_5 = 9988;
    public static final int EP_CHANNEL_6 = 9989;
    public static final int EP_CHANNEL_7 = 9990;

    private EPDataCode() {/**/}
}
